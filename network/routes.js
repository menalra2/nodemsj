const express = require('express');
const message = require('../components/message/message.network');
const user = require('../components/User/user.network');
const chat = require('../components/chat/chat.network');

const routes = function (server) {
    server.use('/message', message);
    server.use('/user', user);
    server.use('/chat', chat);
}

module.exports = routes;