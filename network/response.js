exports.success = (req, res, message, status, internalLog = 'Action Executed') => {
    console.log(`[Server Log Success] ${internalLog}`);
    res.status(status || 200).send({
        error: '',
        body: message,
    });
}

exports.error = (req, res, message, status, internalLog) => {

    console.error(`[Server Log Error] ${internalLog}`);

    res.status(status || 500).send({
        error: message,
        body: '',
    })
}