const db = require('mongoose');
db.Promise = global.Promise;

// 'mongodb+srv://db_user_nodejs:Px3BQitG5mTBh363@cluster0-ewe74.mongodb.net/node_messages'

async function connect (url){

  await db.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  console.log('[Database] connected successfully');
};

module.exports = connect;