const store = require('./message.store');

function addMessage(user, message) {
    return new Promise((resolve, reject) => {
        if (!user || !message) {
            console.error('[messageController] No hay usuario o controller');
            reject('Invalid Data entered');
            return false;
        }
        const infoMessage = {
            user,
            message,
            date: new Date,
        }
        store.add(infoMessage);

        resolve(infoMessage);
    });
};

function getMessages(filteredUser) {
    return new Promise((resolve, reject) => {
        resolve(store.list(filteredUser));
    })
}

function editMessage(id, message) {
    return new Promise(async (resolve, reject) => {
        if (!id || !message) {
            reject('No message or Id');
            return false;
        }
        const result = await store.edit(id, message);
        resolve(result);
    })
}

function deleteMessage(id) {
    return new Promise((resolve, reject) => {
        if (!id) {
            reject('No Id Provided');
            return false;
        }
        store.delete(id)
            .then(() => resolve())
            .catch(e => reject(e))
    })
}

module.exports = {
    addMessage,
    getMessages,
    editMessage,
    deleteMessage,
}