const express = require('express');
const response = require('../../network/response');
const msgController = require('./message.controller');

const router = express.Router();

router.get('/', (req, res) => {

    const filteredMessage = req.query.user || null;
    msgController.getMessages(filteredMessage)
        .then((messages) => {
            response.success(req, res, messages, 200, 'Message retrived');
        })
        .catch(e => {
            response.error(req, res, 'Unexpected error', 500, e);
        })
});

router.post('/', (req, res) => {
    msgController.addMessage(req.body.user, req.body.message)
        .then((infoMessage) => {
            const message = {
                user: infoMessage.user,
                messsage: infoMessage.message,
            };
            response.success(req, res, message, 201);
        })
        .catch(e => {
            response.error(req, res, e, 400, e);
        })
});

router.patch('/:id', (req, res) => {
    msgController.editMessage(req.params.id, req.body.message)
        .then(data => {
            response.success(req, res, data, 200);
        })
        .catch(e => {
            response.error(req, res, 'Error Interno', 500, e);
        });
});

router.delete('/:id', (req, res) => {
    msgController.deleteMessage(req.params.id)
        .then(() => {
            response.success(req, res, `Mensaje ${req.params.id} Eliminado`, 200);
        })
        .catch(e => {
            response.error(req, res, 'Error Interno', 500, e);
        });
});

module.exports = router;