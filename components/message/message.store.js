const Model = require('./message.model');

function addMessage(message) {
  const myMessage = new Model(message);
  myMessage.save();
}

async function getMessages(filteredUser) {
  return new Promise((resolve, reject) => {
    let filter = {};
    if (filteredUser) {
      filter = {
        user: filteredUser,
      }
    }
    Model.find(filter)
      .populate('user')
      .exec((error, payload) => {
        if (error) {
          reject(error);
          return false;
        }
        resolve(payload);
      })
  })

}

async function editText(id, message) {
  const getMessage = await Model.findOne({
    _id: id,
  })
  getMessage.message = message;
  const newMessage = await getMessage.save();
  return newMessage;
}

function deleteMessage(id) {
  return Model.deleteOne({
    _id: id,
  });
}

module.exports = {
  add: addMessage,
  list: getMessages,
  edit: editText,
  delete: deleteMessage,
}