const chatModel = require('./chat.model');

function createChat(users) {
  const newChat = new chatModel(users);
  return newChat.save();
}

async function getChat(userId) {
  return new Promise((resolve, reject) => {
    let filter = {};
    if (userId) filter = { users: userId }

    chatModel.find(filter).populate('users').exec((err, payload) => {
      if (err) {
        reject(err)
        return false;
      }

      resolve(payload);
    });
  });
}

async function getAllChats() {
  return chatModel.find().populate('users');
}

module.exports = {
  create: createChat,
  get: getChat,
  getAll: getAllChats
}
