const chatStore = require('./chat.store');

function createChat(users) {
  if (!Array.isArray(users) || users.length <= 1) return Promise.reject('Invalid data entered');

  const chat = {
    users: users
  }
  return chatStore.create(chat);
}

function getChat(userId) {

  return chatStore.get(userId);
}

function getChats() {
  return chatStore.getAll();
}

module.exports = {
  createChat,
  getChat,
  getChats,
}
