const express = require('express');
const response = require('../../network/response');
const chatController = require('./chat.controller');
const { body, validationResult } = require('express-validator');

const router = express.Router();

router.get('/', async (req, res) => {
  const chats = await chatController.getChats();
  response.success(req, res, chats, 200, 'All chats');
});

router.post('/', [
  //check if the new chat has repeated users
  body('users').custom(users => {
    return new Set(users).size === users.length;
  })
    .withMessage('Repeated users are not allowed'),
  //check if the new chat has more than one user
  body('users').custom(users => {
    return users.length >= 2;
  })
    .withMessage('You need more than one person to make a new chat'),
], (req, res) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });


  const users = req.body.users;
  chatController.createChat(users)
    .then(data => response.success(req, res, data, 201, 'Chat created'))
    .catch(e => response.error(req, res, 'Internal Error', 500, e));
});

// TODO implement a new method to get all chats that belongs to one User
router.get('/:userId', (req, res) => {
  const userId = req.params.userId;
  chatController.getChat(userId)
    .then(chats => response.success(req, res, chats, 200))
    .catch(e => response.error(req, res, 'Internal Error', 500, e));
});

module.exports = router;