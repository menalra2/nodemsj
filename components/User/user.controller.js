const userStore = require('./user.store');

function addUser(user) {
  return userStore.add(user);
}

function getUser(filteredUser) {
  return userStore.list(filteredUser);
}

module.exports = {
  addUser,
  getUser,
}