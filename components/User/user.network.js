const express = require('express');
const response = require('../../network/response');
const userController = require('./user.controller');
const { body, validationResult } = require('express-validator');

const router = express.Router();

router.post('/', [
  body('email').isEmail(),
  body('password').isLength({ min: 8 }),
  body('username').isLength({ min: 6 }),
  body('name').notEmpty(),
], (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });

  const user = {
    email: req.body.email,
    password: req.body.password,
    username: req.body.username,
    name: req.body.name,
    dateOfBirth: req.body.dateOfBirth,
  }

  userController.addUser(user)
    .then(data => response.success(req, res, data, 201, 'New user created'))
    .catch(e => response.error(req, res, e, 500, 'Invalid Entry'));
});

router.get('/', (req, res) => {
  const filteredMessage = req.query.name || null;
  let internalMsg = '';
  filteredMessage !== null ? internalMsg = 'individual user retrieved' : internalMsg = 'all users retrieved';

  userController.getUser(filteredMessage)
    .then(users => response.success(req, res, users, 200, internalMsg))
    .catch(e => response.error(req, res, 'Internal Error', 500, e));
})

module.exports = router;