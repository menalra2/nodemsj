const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: String,
  password: String,
  username: String,
  name: String,
  dateOfBitrh: Date,
});

const model = mongoose.model('user', userSchema);
module.exports = model;