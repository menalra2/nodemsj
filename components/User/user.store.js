const userModel = require('./user.model');


async function addUser(user) {
  const newUser = new userModel(user);
  const existentUser = await listUSer(user.username);
  console.log(existentUser.length);
  if (existentUser.length <= 0) return newUser.save();
  else return Promise.reject('This user already exist');
}

async function listUSer(filteredUser) {
  let filter = {};
  if (filteredUser) {
    filter = {
      username: filteredUser,
    }
  }
  const users = await userModel.find(filter);
  return users;
}


module.exports = {
  add: addUser,
  list: listUSer,
  // edit: editUser,
  // delete: deleteUser,
}